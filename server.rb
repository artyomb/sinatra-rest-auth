require 'sinatra'
require_relative 'rest-app'
require 'jwt'
require 'sequel'
require 'sqlite3'
require 'rest-client'

SIGNING_KEY = OpenSSL::PKey::RSA.generate 2048
VERIFY_KEY = SIGNING_KEY.public_key
JWT_ISSUER = 'Authority'

Sequel::Model.plugin :json_serializer
DB = Sequel.sqlite

DB.create_table? :users do
  primary_key :id
  String :login
  String :password
end

class User < Sequel::Model; end

User.create login: 'user1', password: 'secret'


class DemoApi < RestApp

  get "/create_token", authorize: false do
    halt 401 unless User.find login: rest_params[:login], password: rest_params[:password]

    JWT.encode({
      iat: Time.now.to_i, exp: Time.now.to_i + 60 * 60, iss: JWT_ISSUER,
      scopes: %w[add_money remove_money view_money],
      user: { username: rest_params[:login] }}, SIGNING_KEY, 'RS256')
  end

  get "/users" do
    User.all
  end

end

Thread.new do
  sleep 0.1
  token = JSON.parse RestClient.get 'localhost:4567/create_token', {params: {login: 'user1', password: 'secret'}}
  puts JWT.decode token, VERIFY_KEY, true, { algorithm: 'RS256'}
  puts RestClient.get 'localhost:4567/users', {authorization: "Bearer #{token}"}
  exit
end
DemoApi.run!
