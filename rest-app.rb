

class RestApp < Sinatra::Application
  def authorize
    bearer = env.fetch('HTTP_AUTHORIZATION').slice(7..-1)
    payload = JWT.decode bearer, VERIFY_KEY, true, { algorithm: 'RS256'}
  end

  def self.get(path, opts = {}, &block)
    super path, opts.reject{|k,v| k == :authorize} do
      authorize unless opts[:authorize] == false
      content_type :json
      instance_exec(&block).to_json
    end
  end

  def rest_params
    data = params # rack.request.form_hash', 'rack.request.form_imput', 'rack.tempfile'
    if @env['CONTENT_TYPE'] =~ /application\/json/
      body_str = request.body.read
      body_str.force_encoding 'utf-8'
      data = body_str.empty? ? {} : JSON.parse(body_str, symbolize_names: true)
    end
    data
  end

end